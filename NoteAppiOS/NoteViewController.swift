//
//  ViewController.swift
//  NoteAppiOS
//
//  Created by hongly on 12/10/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var takeNoteBarButton: UIBarButtonItem!
    
    var noteModelController: NoteModelController = NoteModelController.noteModelController
    
    //Datasource for CollectionView
    var notes = Array<Note>()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        //Disble default gesture of Navigation Controller
        self.navigationController!.interactivePopGestureRecognizer?.isEnabled = false;
        
        notes = noteModelController.getAllNotes()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: Notification.Name("languageChanged"), object: nil)
        

        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.collectionView.addGestureRecognizer(lpgr)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        changeLanguage()
        notes = noteModelController.getAllNotes()
        collectionView.reloadData()
    }

    @IBAction func takeNoteTapped(_ sender: Any) {
      performSegue(withIdentifier: "navigateToEdit", sender: self)
    }
    
    //When user tapped to change language
    @IBAction func changeLanguageTapped(_ sender: UIBarButtonItem) {
        let chooseLangAlert = UIAlertController(title: "choose language".localized, message: "", preferredStyle: .actionSheet)
        chooseLangAlert.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            LanguageManager.shared.language = "en"
            NotificationCenter.default.post(name: Notification.Name("languageChanged"), object: nil)
        }))
        chooseLangAlert.addAction(UIAlertAction(title: "ខ្មែរ", style: .default, handler: { (action) in
            LanguageManager.shared.language = "km"
            NotificationCenter.default.post(name: Notification.Name("languageChanged"), object: nil)
        }))
        
        present(chooseLangAlert, animated: true, completion: nil)
    }
    
    @objc func changeLanguage() {
        takeNoteBarButton.title = "take a note...".localized
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            return
        }
        
        let p = gestureReconizer.location(in: self.collectionView)
        let indexPath = self.collectionView.indexPathForItem(at: p)
        
        if let index = indexPath {
            //var cell = self.collectionView.cellForItem(at: index)
           
            let deleteAlert = UIAlertController(title: "delete".localized
                , message: "do you want to delete?".localized, preferredStyle: .alert)
            deleteAlert.addAction(UIAlertAction(title: "delete".localized, style: .destructive, handler: { (_) in
                self.noteModelController.deleteNoteById(id: self.notes[index.row].id!)
                
                self.notes = self.noteModelController.getAllNotes()
                self.collectionView.reloadData()
            }))
            deleteAlert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: { (_) in
                return
            }))
            
            present(deleteAlert, animated: true, completion: nil)
            
        } else {
            print("Could not find index path")
        }
    }
}


extension NoteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width - 30
        let orientation = UIApplication.shared.statusBarOrientation

        if orientation == .landscapeLeft || orientation == .landscapeRight  {
            return CGSize(width: width / 3 , height: width / 3)
        }else {
            return CGSize(width: width / 2 , height: width / 2)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.register(UINib(nibName: "NoteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "note")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "note", for: indexPath) as! NoteCollectionViewCell
        
        cell.titleLabel.text = notes[indexPath.row].title
        cell.descriptionLabel.text = notes[indexPath.row].descr
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "editViewController") as! EditViewController
        vc.note = noteModelController.getOneNoteById(id: notes[indexPath.row].id!)
        vc.isUpdate = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    

}
