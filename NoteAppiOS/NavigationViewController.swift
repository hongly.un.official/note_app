//
//  NavigationViewController.swift
//  NoteAppiOS
//
//  Created by hongly on 12/10/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    var noteModelController: NoteModelController?
    
    override func viewDidLoad()  {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let noteViewController = segue.destination as? NoteViewController {
//            noteViewController.noteModelController = noteModelController
//        }
//    }
    



}
