//
//  EditViewController.swift
//  NoteAppiOS
//
//  Created by hongly on 12/10/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    var noteModelController: NoteModelController = NoteModelController.noteModelController

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    var note:Note?
    var isUpdate: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        changeLanguage()
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: Notification.Name("languageChanged"), object: nil)
        
        if isUpdate!{
            if note != nil {
                titleTextField.text = note?.title
                descriptionTextView.text = note?.descr
            }
        }else{
            setTextViewPlaceholder(value: "description".localized)
        }
    }
    
    @objc func changeLanguage() {
        titleTextField.placeholder = "title".localized
        descriptionTextView.text = "description".localized
    }
    
    func setTextViewPlaceholder(value: String)  {
        descriptionTextView.textColor = UIColor.lightGray
        descriptionTextView.text = value
    }
    
    
    @IBAction func swipeToSave(_ sender: UISwipeGestureRecognizer) {
        
        guard let title = titleTextField.text else {
            return
        }
        guard let description = descriptionTextView.text else {
            return
        }
        
        if isUpdate! {
            note?.title = titleTextField.text
            note?.descr = descriptionTextView.text
            noteModelController.updateNoteById(newNote: note!)
            isUpdate = false
        }else{
            noteModelController.insert(title: title, description: description )
        }
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
}


extension EditViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.text == "description".localized {
            descriptionTextView.textColor = UIColor.black
            descriptionTextView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if descriptionTextView.text == "" {
            descriptionTextView.textColor = UIColor.lightGray
            descriptionTextView.text = "description".localized
        }
    }
}
