//
//  NoteModelController.swift
//  NoteApp
//
//  Created by hongly on 12/10/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import CoreData

class NoteModelController {
    
    
    static var noteModelController = NoteModelController()
    
    
    func insert(title: String, description: String) {
        let context = AppDelegate.viewContext
        let note = Note(context: context)
        
        note.id = UUID.init().uuidString
        note.title = title
        note.descr = description
        
        do {
            try context.save()
        } catch let err {
            print("Insert Error")
            print(err)
        }
    }
    
    func getOneNoteById(id: String) -> Note?{
        let context = AppDelegate.viewContext
        let request:NSFetchRequest<Note> = Note.fetchRequest()
        
        var note: Note?
        
        do {
            let fetchResult = try context.fetch(request)
            for noteById in fetchResult where noteById.id == id{
                note = noteById
            }

        } catch let err {
            print("Fetch Error")
            print(err)
        }
        
        return note
    }
    
    
    func getAllNotes() -> [Note]{
        let context = AppDelegate.viewContext
        let request:NSFetchRequest<Note> = Note.fetchRequest()
        
        var notes = Array<Note>()
        
        do {
            notes = try context.fetch(request)
        } catch let err {
            print("Fetch Error")
            print(err)
        }
        
        return notes
    }
    
    func updateNoteById(newNote: Note){
        
        let context = AppDelegate.viewContext
        let request:NSFetchRequest<Note> = Note.fetchRequest()
        
        do {
            let fetchResult = try context.fetch(request)
            for note in fetchResult where note.id == newNote.id{
                note.title = newNote.title
                note.descr = newNote.descr
            }
            try context.save()
        } catch let err {
            print("Update Note Error")
            print(err)
        }
    }
    
    func deleteNoteById(id: String){
        
        let context = AppDelegate.viewContext
        let request:NSFetchRequest<Note> = Note.fetchRequest()
        
        do {
            let fetchResult = try context.fetch(request)
            for note in fetchResult where note.id == id{
                context.delete(note)
            }
            try context.save()
        } catch let err {
            print("Update Note Error")
            print(err)
        }
    }
}
